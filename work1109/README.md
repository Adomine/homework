# About the training set
----------
The data set for training is too large and can be accessed through the following links: https://data.caltech.edu/records/mzrjq-6wc02

Place the file "101_ObjectCategories.tar.gz" in the home directory when the download is complete.